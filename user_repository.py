# -*- coding: utf-8 -*-


from abc import ABC
from abc import abstractmethod

from user_entity import User


class UserRepository(ABC):
    """
    UserRepository
    """

    @abstractmethod
    def create(self, user_entity: User):
        """
        create
        :param user_entity:
        :return: User
        """

        raise NotImplemented("Not implemented")
