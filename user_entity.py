# -*- coding: utf-8 -*-


from dataclasses import dataclass


@dataclass
class User:
    """
    User
    """

    first_name: str
    last_name: str
    email: str
    password: str
