# -*- coding: utf-8 -*-


from mail_sender import EmailSender
from user_entity import User
from user_repository import UserRepository


class UserCreator:
    """
    UserCreator
    """

    def __init__(self, user_repository: UserRepository, email_sender: EmailSender):
        """
        UserCreator
        :param user_repository:
        """

        self.__user_repository = user_repository
        self.__email_sender = email_sender

    def __call__(
        self,
        first_name: str,
        last_name: str,
        email: str,
        password: str,
        password_confirmation,
    ):
        """
        __call__
        :param first_name:
        :param last_name:
        :param email:
        :param password:
        :param password_confirmation:
        :return:
        """

        if password != password_confirmation:
            raise ValueError("Password doesn't match password confirmation")

        new_user = User(
            first_name=first_name,
            last_name=last_name,
            email=email,
            password=password,
        )

        self.__user_repository.create(new_user)

        self.__email_sender.send(
            subject="",
            content="",
            send_from="",
            send_to="",
        )
