# -*- coding: utf-8 -*-


from user_entity import User
from user_repository import UserRepository


class MockExternalDBUserRepository(UserRepository):
    """
    MockExternalDBUserRepository
    """

    def create(self, user_entity: User):
        """
        create
        :param user_entity:
        :return: User
        """

        print(f"User with email:{user_entity.email} created!")
