#!/usr/bin/env python3


from mock_externaldb_user_repository import MockExternalDBUserRepository
from user_creator import UserCreator


if __name__ == "__main__":

    externaldb_user_repository = MockExternalDBUserRepository()

    user_creator = UserCreator(
        user_repository=externaldb_user_repository,
        email_sender={},
    )

    user_creator(
        first_name="",
        last_name="",
        email="",
        password="",
        password_confirmation="",
    )
