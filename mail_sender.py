# -*- coding: utf-8 -*-


from abc import ABC
from abc import abstractmethod


class EmailSender(ABC):
    """
    EmailSender
    """

    @abstractmethod
    def send(self, subject: str, content: str, send_from: str, send_to: str):
        """
        send
        :param subject:
        :param content:
        :param send_from:
        :param send_to:
        :return:
        """

        raise NotImplemented("Not implemented")
